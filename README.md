# Installation

* Install Dependency pandas: `pip3 install pandas`
* Install Dependency lxml: `pip3 install lxml`
* Install Dependency lxml: `pip3 install openpyxl`

# Konfiguration

## config.json

### Standardbeispiel für die Mapping-Konfiguration "einfacher" Metadaten (inkl. Dublettencheck):
Hier werden den ausgewählten Datensätzen die beiden Felder RestrictionOnAccessLicense und UseAndReproductionLicense hinzugefügt.
```json
{
    "inputFileName" : "InputFileName.xlsx",
    "metadataDir" : "metadata/",
    "startRow" : 2,
    "endRow" : 300,
    "mapping":
    [
        {
            "ugh": "RestrictionOnAccessLicense",
            "header": "RestrictionOnAccessLicense"
        },
        {
            "ugh": "UseAndReproductionLicense",
            "header": "UseAndReproductionLicense"
        }
    ]
}
```

### Beispiel für abweichenden Einhängepunkt für das neue Metadatum
```json
{
    "inputFileName" : "InputFileName.xlsx",
    "metadataDir" : "metadata/",
    "startRow" : 2,
    "endRow" : 300,
    "mapping":
    [
       {
        "ugh": "QID",
        "header": "QID_Person",
        "xPathFind" : ".//mods:extension/goobi:goobi/goobi:metadata[@name='Balthasar']"
        }
    ]
}
```

### Beispiel Mapping-Config für das Replacement bei Personendaten, basierend auf GND-Matching
```json
        {
            "normDataHeader" : "Person_GND",
            "normDataAuthority" : "gnd",
            "type": "person",
            "ugh" : "SubjectPerson",
            "firstNameHeader" : "Person_Vorname",
            "lastNameHeader" : "Person_Nachname",
            "termsOfAddressHeader" : "Person_Namenszusatz",
            "displayNameHeader" : "Person_Ansetzung", 
            "dateHeader" : "Person_Lebensdaten",              
            "header" : "Person_Ansetzung",
            "replaceExisting": true,
            "xPathFind" : ".//mods:extension/goobi:goobi/goobi:metadata[@name='SubjectPerson']",
            "replaceMatchUGH" : "authorityValue",
            "replaceMatchHeader" : "Person_GND"
        }
```

### Beispiel Mapping-Config für das Hinzufügen neuer Personendaten
```json
        {
            "normDataHeader" : "Person_QID",
            "normDataAuthority" : "wikidata",
            "type": "person",
            "ugh" : "Creator",
            "firstNameHeader" : "Person_Vorname",
            "lastNameHeader" : "Person_Nachname",
            "termsOfAddressHeader" : "Person_Namenszusatz",
            "displayNameHeader" : "Person_Ansetzung", 
            "dateHeader" : "Person_Lebensdaten",              
            "header" : "Person_Ansetzung"
        }   
```

### Beispiel für das Ändern des DocStrctTypes
```json
    "mapping":
    [
        {
            "header": "DocStrctType",
            "replaceExisting": true,
            "type" : "DocStrctType",
            "xPathFind" : ".//mets:div[@DMDID='DMDLOG_0000']"
        }
    ]
```

# Postproduction in Goobi

```yaml
---
# This GoobiScript ensures that the internal database table of the Goobi database is updated with the status of the workflows and the associated media files as well as metadata from the METS file.
action: updateDatabaseCache

```