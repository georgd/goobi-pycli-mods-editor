import pandas as pd
import json
import addMods as Mods
import glob

# read and parse ConfigFile
with open('config.json', 'r') as myfile:
    data=myfile.read()
config = json.loads(data)
#print(config)
# metadataFolder-Path
metadataDir = config["metadataDir"]

#inputFileName = input("Bitte Input-XLS-Dateinamen (inkl. Endung) angeben: ")
inputFileName = config["inputFileName"]
data = pd.read_excel(inputFileName)
#df = pd.DataFrame(data, columns=['VorgangID','UseAndReproductionLicense'])
df = pd.DataFrame(data)
df = df.iloc[(config["startRow"]-2):(config["endRow"]-1)]
#print (df)

#Schleife durch das Excel-Input-File, konkret den pandaisierten DataFrame.
for index, row in df.iterrows():
    processId = str(row["VorgangID"])
    #Innere Schleife durch das Metadata/Header-Mapping in der config.json für jede Datenreihe
    for configData in config["mapping"]:
        try:
            print(str(row["VorgangID"])+": add "+row[configData["header"]]+" for MODS "+configData["ugh"])
        except KeyError:
            print(str(row["VorgangID"])+": add "+configData["header"]+" "+row[configData["header"]])
        try:
            print(f"::: add Normdata with {row[configData['normDataHeader']]} for {configData['normDataHeader']}")
        except:
            pass

    #Einlesen des meta.xml-Files für den angesprochenen Vorgang
    filename = f"{metadataDir}{processId}/meta.xml"
    modsTree = Mods.parseMetadata(filename)
        
    for configData in config["mapping"]:
        noReplaceExistingMods = False
        # Prüfen ob im Config-Mapping replaceExisting definiert wurde, oder ob default das Metadatum mit Dublettenkontrolle ergänzt werden soll
        try:
            if configData["replaceExisting"] == True:
                #print(f"noReplace: {noReplaceExistingMods}")
                #Ersetzen von bestehenden Metadaten
                try:
                    print(configData["ugh"])
                    xpathFindStr = f"{configData['xPathFind']}[goobi:{configData['replaceMatchUGH']}[text()='{row[configData['replaceMatchHeader']]}']]"
                except KeyError:
                    xpathFindStr = f"{configData['xPathFind']}"
                print(xpathFindStr)
                findMetadata = Mods.findMetadata(modsTree,xpathStr=xpathFindStr)
                print(findMetadata)
                Mods.backupMetaFile(processId,modsTree,metadataDir)
                try:
                    parent = findMetadata[0].getparent()
                    parent.remove(findMetadata[0])
                    print(Mods.writeNewMetaFile(processId,modsTree,metadataDir))
                    print("delete current node and write a new one")
                    try:
                        if configData["type"] == "person":
                            print("add new Person typed block")
                            modsTree = Mods.addPersonMetadata(modsTree,row,configData)
                            Mods.writeNewMetaFile(processId,modsTree,metadataDir)
                        elif configData["type"] == "DocStrctType":
                            print("change DocStrctType")
                            modsTree = Mods.addMETSDocStrct(modsTree,row,configData)
                            Mods.writeNewMetaFile(processId,modsTree,metadataDir)
                    except KeyError:
                        print ("no specific metadataType defined")
                except IndexError:
                    print("nothing to delete")
                    pass
            else:
                #replaceExisting im Config-Mapping steht auf False
                noReplaceExistingMods = True
        except KeyError:
            #Der Key replaceExisting ist im Config-Mapping nicht definiert, somit default -> kein Replacement
            noReplaceExistingMods = True

        if noReplaceExistingMods == True:
            #Einfügen neuer Metadaten, mit voriger Dublettenkontrolle (modsExists)
            try:
                xpathFindStr = f"{configData['xPathFind']}/goobi:metadata[@name='{configData['ugh']}'][text()='{row[configData['header']]}']"
                xpathFindPath = f"{configData['xPathFind']}"
            except:
                xpathFindStr = ""
                xpathFindPath = ""
            modsExists = Mods.findMetadata(modsTree, row[configData["header"]], configData["ugh"],xpathStr=xpathFindStr)
            # Wenn modsExists == 0 -> Metadatum mit gegebenem Wert noch nicht vorhanden, dann Anlegen der Daten
            if modsExists == []:
                Mods.backupMetaFile(processId,modsTree,metadataDir)
                #Authority-Handling
                try:
                    normdata = {}
                    normdata["value"] = row[configData['normDataHeader']]
                    normdata["authority"] = configData['normDataAuthority']
                except:
                    normdata = ""
                # Check ob spezieller MD-Type verhandelt wird:
                try:
                    if configData["type"] == "person":
                        print("add new Person typed block")
                        modsTree = Mods.addPersonMetadata(modsTree,row,configData)
                except KeyError:
                    #Standard-falches Metadatum ergänzen:
                    modsTree = Mods.addMetadata(modsTree, row[configData["header"]], configData["ugh"], normdata, xpathFindStr=xpathFindPath)
                Mods.writeNewMetaFile(processId,modsTree,metadataDir)
            else:
                print("already inserted: "+(processId)+": add "+row[configData["header"]]+" for MODS "+configData["ugh"])