import lxml.etree as ET
from datetime import datetime
import pandas as pd

def parseMetadata(filename):
    #mytree = ET.parse(f'metadata/{processId}/meta.xml',parser=ET.XMLParser(remove_blank_text=True))
    mytree = ET.parse(filename, parser=ET.XMLParser(remove_blank_text=True))
    mytree = mytree.getroot()
    return mytree

def getNewMetadataElement(mytree, metadata='{http://meta.goobi.org/v1.5.1/}metadata', xpathFindStr=""):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    ET.register_namespace=namespaces
    if xpathFindStr == "":
        xpathFindStr = './/mods:extension/goobi:goobi'
    #print(xpathFindStr)
    e = mytree.find(xpathFindStr, namespaces)
    try:
       newMetadata = ET.SubElement(e, metadata)
       return newMetadata
    except TypeError:
        return None


def addMetadata(mytree, value, attributeName, normdata="", xpathFindStr=""):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    ET.register_namespace=namespaces
    if xpathFindStr=="":
        xpathFindStr='.//mods:extension/goobi:goobi'
    newMetadata = getNewMetadataElement(mytree,xpathFindStr=xpathFindStr)
    
    newMetadata.text = value
    newMetadata.set("name", attributeName)
    if normdata != "":
        if normdata["authority"] == "gnd": #authority="gnd" authorityURI="http://d-nb.info/gnd/" name="SubjectPerson" valueURI="http://d-nb.info/gnd/120409569"
            newMetadata.set("authority", "gnd")
            newMetadata.set("authorityURI", "http://d-nb.info/gnd")
            newMetadata.set("valueURI", f"http://d-nb.info/gnd/{normdata['value']}")
    print(f"new insert: add {value} for MODS {attributeName}")
    return mytree

def addPersonMetadata(mytree, data, config):
    #print(f"Daten: {data}")
    #print(f"Config: {config}")
    newMetadata = getNewMetadataElement(mytree)
    print(newMetadata)
    newMetadata.set("name", config["ugh"])
    newMetadata.set("type", config["type"])

    personMetadataParts = ["firstName", "lastName", "termsOfAddress", "date", "displayName"]
    for personMetadata in personMetadataParts:
        if pd.isna(data[config[f"{personMetadata}Header"]]) == False:
            namePart = ET.SubElement(newMetadata, f'{{http://meta.goobi.org/v1.5.1/}}{personMetadata}')
            namePart.text = data[config[f"{personMetadata}Header"]]

    try:
        authority = config["normDataAuthority"]
    except KeyError:
        authority = ""

    if authority == "gnd":
        authorityID = ET.SubElement(newMetadata, f'{{http://meta.goobi.org/v1.5.1/}}authorityID')
        authorityID.text = "gnd"
        authorityURI = ET.SubElement(newMetadata, f'{{http://meta.goobi.org/v1.5.1/}}authorityURI')
        authorityURI.text = "http://d-nb.info/gnd/"
        authorityValue = ET.SubElement(newMetadata, f'{{http://meta.goobi.org/v1.5.1/}}authorityValue')
        authorityValue.text = str(data[config["normDataHeader"]])
    elif authority == "wikidata":
        authorityID = ET.SubElement(newMetadata, f'{{http://meta.goobi.org/v1.5.1/}}authorityID')
        authorityID.text = "wikidata"
        authorityURI = ET.SubElement(newMetadata, f'{{http://meta.goobi.org/v1.5.1/}}authorityURI')
        authorityURI.text = "https://www.wikidata.org/entity/"
        authorityValue = ET.SubElement(newMetadata, f'{{http://meta.goobi.org/v1.5.1/}}authorityValue')
        authorityValue.text = str(data[config["normDataHeader"]])     
    return mytree

def addMETSDocStrct(mytree, data, config):
    xpathFindStr='.//mets:structMap[@TYPE="LOGICAL"]'
    newMetadata = getNewMetadataElement(mytree, metadata='{http://www.loc.gov/METS/}div', xpathFindStr=xpathFindStr)
    # <mets:div DMDID="DMDLOG_0000" ID="LOG_0000" TYPE="Object"/>
    newMetadata.set("DMDID", "DMDLOG_0000")
    newMetadata.set("ID", "LOG_0000")
    newMetadata.set("TYPE", data[config["header"]])
    return mytree

def findMetadata(mytree, value="", attributeName="", xpathStr = ""):
    namespaces = {'mods' : 'http://www.loc.gov/mods/v3','mets' : 'http://www.loc.gov/METS/',  'goobi' :  'http://meta.goobi.org/v1.5.1/' }
    ET.register_namespace=namespaces
    if xpathStr == "":
        xpathStr = f".//goobi:metadata[@name='{attributeName}'][text()='{value}']"
    #print(xpathStr)
    e = mytree.xpath(xpathStr, namespaces=namespaces)
    #print(e)
    return e

def writeNewMetaFile(processId, mytree, filePath):
    mytree = ET.ElementTree(mytree)
    mytree.write(f'{filePath}{processId}/meta.xml',encoding="UTF-8", xml_declaration=True, pretty_print=True)

def backupMetaFile(processId, mytree, filePath):
    mytree = ET.ElementTree(mytree)
    currentDateStr = datetime.utcnow().strftime('%Y-%m-%d-%H%M%S%f')[:-3]
    mytree.write(f'{filePath}{processId}/meta.xml.{currentDateStr}', encoding="UTF-8", xml_declaration=True, pretty_print=True)
